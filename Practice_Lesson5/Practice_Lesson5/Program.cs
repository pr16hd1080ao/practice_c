﻿using System;

namespace Practice_Lesson5
{
    class Program
    {
        static void Main(string[] args)
        {
            void sum()
            {
                int a;
                int b;
                int sum;
                Console.WriteLine("В этой функции мы вычисляем сумму двух чисел.");
                Console.Write("Введите первое число:");
                a = Convert.ToInt32(Console.ReadLine());
                Console.Write("Введите второе число:");
                b = Convert.ToInt32(Console.ReadLine());
                sum = a + b;
                Console.WriteLine($"Сумма двух чисел равняется {sum}.");
            }

            void rectangleSquare()
            {
                int a;
                int b;
                int square;
                Console.WriteLine("В этой функции мы вычисляем площадь прямоугольника.");
                Console.Write("Введите длину первой стороны:");
                a = Convert.ToInt32(Console.ReadLine());
                Console.Write("Введите длину второй стороны:");
                b = Convert.ToInt32(Console.ReadLine());
                square = a * b;
                Console.WriteLine($"Площадь прямоугольника равняется {square}.");
            }

            void circleSquare()
            {
                int a;
                double p = 3.14;
                double square;
                Console.WriteLine("В этой функции мы вычисляем площадь круга.");
                Console.Write("Введите радиус круга:");
                a = Convert.ToInt32(Console.ReadLine());
                square = a * p * p;
                Console.WriteLine($"Площадь круга равняется {square}.");
            }

            void oddEven()
            {
                int a;
                double result;
                Console.WriteLine("В этой функции мы определяем является число чётным или нечётным.");
                Console.Write("Введите число:");
                a = Convert.ToInt32(Console.ReadLine());
                result = a % 2;
                
                if(result == 0)
                {
                    Console.WriteLine("Ваше число чётное.");
                }
                else
                {
                    Console.WriteLine("Число нечётное");
                }

            }

            void arraySum()
            {
                int[] numbers = new int[5];
                Console.WriteLine("В этой функции мы находим сумму чисел в массиве, размером в 5 символов.");
                Console.WriteLine("Введите 5 чисел массива");
                numbers[0] = Convert.ToInt32(Console.ReadLine());
                numbers[1] = Convert.ToInt32(Console.ReadLine());
                numbers[2] = Convert.ToInt32(Console.ReadLine());
                numbers[3] = Convert.ToInt32(Console.ReadLine());
                numbers[4] = Convert.ToInt32(Console.ReadLine());
                int sum = 0;
                for (int i = 0; i < numbers.Length; i++)
                {
                    sum = sum + numbers[i];
                }
                Console.WriteLine("Сумма элементов массива равна " + sum);
            }

            sum();
            Console.WriteLine();
            rectangleSquare();
            Console.WriteLine();
            circleSquare();
            Console.WriteLine();
            oddEven();
            Console.WriteLine();
            arraySum();
        }
    }
}
